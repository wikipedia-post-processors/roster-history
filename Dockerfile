FROM node:14-alpine
WORKDIR /app
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
COPY app.js /app/app.js
COPY stats.json /app/stats.json
COPY views/ /app/views/
COPY public/ /app/public/
RUN npm install

EXPOSE 2143

ENTRYPOINT ["node", "app.js"]
let nba = document.getElementById("nba");
let wnba = document.getElementById("wnba");

document.addEventListener("DOMContentLoaded", function () {
  // Get current stats
  let req = new XMLHttpRequest();
    req.open('GET', `http://rosterhistory.loafing.dev/api/usage-stats`, true);
    req.addEventListener('load', function () {
        let response = JSON.parse(req.responseText);
        console.log(response)
        nba.innerText = `queries: ${response.NBA.queries}, ${response.NBA.percent}%`
        wnba.innerText = `queries: ${response.WNBA.queries}, ${response.WNBA.percent}%`     
    });
  req.send();
});
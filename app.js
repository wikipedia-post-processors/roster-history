const express = require('express');
const app = express();
const handlebars = require('express-handlebars').create({defaultLayout:'main'});
const bodyParser = require('body-parser');
const path = require('express-path')
const fs = require('fs');
const fetch = require('node-fetch');
const nba_scrapper = process.argv[2]
const wnba_scrapper = process.argv[3]


app.use("/", express.static(__dirname + '/public'))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
app.set('port', 2143);

app.get("/", function(req,res){
  let data = {}
  data.css = ['home.css']
  data.js = ['home.js']
  res.render('home', data)
});

app.get("/api/roster-history", async function(req,res){
  const league = req.query.league;
  const city = req.query.city;
  const team = req.query.team
  let start = req.query.start;
  start = parseInt(start);
  let end = req.query.end;
  end = parseInt(end);
  var result  = {};

  if (!(league === 'NBA' || league === 'WNBA')) {
    result.code = 401
    result.message = 'Invalid league'
    res.send(result)
    return;
  }

  if (!(typeof start === 'number' || typeof end === 'number')) {
    result.code = 402
    result.message = 'Start and end years are not numbers'
    res.send(result);
    return;
  }

  if (start > end) {
    result.code = 403
    result.message = 'Start year greater than end year'
    res.send(result);
    return;
  }

  if (league === "NBA") {
    UpdateStats(league);
    console.log("stats updated")

    result.city = city;
    result.team = team;
    result.start = start;
    result.end = end;

    let years = [];
    for (year = start; year <= end; year++) {
      years.push(year)
    }

    for (const year of years) {  
      const response = await fetch(`${nba_scrapper}/api/roster?teamcity=${city}&teamname=${team}&year=${year}`);
      if (response.status < 400) {
        const roster_data = await response.json();
        result[`${year}`] = roster_data.roster;
      } else {
        result[`${year}`] = ['Roster not found'];
      }
    }
    result.code = 200
    result.message = 'Success'
    res.send(result)
  }

  if (league === "WNBA") {
    UpdateStats(league);
    console.log("stats updated")

    result.city = city;
    result.team = team;
    result.start = start;
    result.end = end;

    let years = [];
    for (year = start; year <= end; year++) {
      years.push(year)
    }

    for (const year of years) {  
      const response = await fetch(`${wnba_scrapper}/api/roster?teamcity=${city}&teamname=${team}&year=${year}`);
      if (response.status < 400) {
        const roster_data = await response.json();
        result[`${year}`] = roster_data.roster;
      } else {
        result[`${year}`] = ['Roster not found'];
      }
    }
    result.code = 200
    result.message = 'Success'
    res.send(result)
  }
  
  if (!result.hasOwnProperty('code')) {
    result.code = 400
    result.message = 'Bad request'
    res.send(result) 
  }
});

app.get("/api/usage-stats", function(req,res){
  console.log("request received")

  let raw_stats = fs.readFileSync('stats.json')
  let stats = JSON.parse(raw_stats);

  res.send(stats)
});

app.get("/api/player-stats", async function(req,res){
  const f_name = req.query.f_name;
  const l_name = req.query.l_name;
  const year = req.query.year
  var result  = {};

  const response = await fetch(`${wnba_scrapper}/api/player/year/${year}?firstname=${f_name}&lastname=${l_name}`);
  if (response.status < 400) {
    const stats = await response.json();
    result["stats"] = stats.stats;
  } else {
    result["stats"] = ["Stats not found"];
  }
  result.code = 200
  result.message = 'Success'
  res.send(result)
});

function UpdateStats(league){
  let raw_stats = fs.readFileSync('stats.json')
  let stats = JSON.parse(raw_stats);
  
  stats[league].queries += 1;
  stats.NBA.percent = Math.round(100 * (stats.NBA.queries / (stats.NBA.queries + stats.WNBA.queries)));
  stats.WNBA.percent = Math.round(100 * (stats.WNBA.queries / (stats.NBA.queries + stats.WNBA.queries)));

  let write_stats = JSON.stringify(stats);
  fs.writeFileSync('stats.json', write_stats);
}

// Error handling
app.use(function(req,res){
  res.status(404);
  res.render('404');
});

app.use(function(err, req, res, next){
  console.error(err.stack);
  res.type('plain/text');
  res.status(500);
  res.render('500');
});

app.listen(app.get('port'), function(){
  console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});